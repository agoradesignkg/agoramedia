<?php

namespace Drupal\agoramedia;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Swaps the oembed resource fetcher service (media.oembed.resource_fetcher).
 */
class AgoramediaServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('media.oembed.resource_fetcher');
    $definition->setClass('Drupal\agoramedia\OEmbed\HqResourceFetcher');
  }

}
