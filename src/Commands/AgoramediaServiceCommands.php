<?php

namespace Drupal\agoramedia\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush service commands.
 */
class AgoramediaServiceCommands extends DrushCommands {

  /**
   * The default database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $database;

  /**
   * The media storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected ContentEntityStorageInterface $mediaStorage;

  /**
   * Constructs a new AgoramediaServiceCommands object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(Connection $database, EntityTypeManagerInterface $entity_type_manager) {
    $this->database = $database;
    $this->mediaStorage = $entity_type_manager->getStorage('media');
  }

  /**
   * Lists unreferenced media entities.
   *
   * Note that entity_usage module must be installed and properly configured in
   * order to get a valid result here.
   *
   * @command agoramedia:list-unref-media
   *
   * @usage agoramedia:list-unref-media
   *   Lists unreferenced media entities.
   *
   * @aliases agm:lum
   *
   * @return \Consolidation\OutputFormatters\StructuredData\RowsOfFields|null
   *   List of media entities formatted as table.
   */
  public function listUnreferencedMediaEntities() {
    $this->output()->writeln('Take care that entity_usage must be properly configured to get valid results, eg do not forget to consider base fields!!');
    $media_ids = $this->getUnreferencedMediaIds();
    $this->output()->writeln(sprintf('Found %s unreferenced media entities', count($media_ids)));

    if (!empty($media_ids)) {
      $output = [];
      /** @var \Drupal\media\MediaInterface[] $media_entities */
      $media_entities = $this->mediaStorage->loadMultiple($media_ids);
      foreach ($media_entities as $media_entity) {
        $output[] = [
          'id' => $media_entity->id(),
          'label' => $media_entity->label(),
          'changed' => $media_entity->getChangedTime(),
          'created' => $media_entity->getCreatedTime(),
        ];
      }
      return new RowsOfFields($output);
    }
  }

  /**
   * Deletes unreferenced media entities.
   *
   * USE WITH CAUTION!
   *
   * @command agoramedia:delete-unreferenced-media
   *
   * @usage agoramedia:delete-unreferenced-media
   *   Deletes unreferenced media entities.
   */
  public function deleteUnreferencedMediaEntities() {
    $media_ids = $this->getUnreferencedMediaIds();
    if (!empty($media_ids)) {
      /** @var \Drupal\media\MediaInterface[] $media_entities */
      $media_entities = $this->mediaStorage->loadMultiple($media_ids);
      $this->mediaStorage->delete($media_entities);
      $this->writeln(sprintf('Deleted %s unused media entities.', count($media_entities)));
    }
  }

  /**
   * Queries unreferenced media IDs.
   *
   * @return array
   *   The unreferenced media IDs.
   */
  protected function getUnreferencedMediaIds(): array {
    $query = $this->database->select('media', 'm');
    $query->fields('m', ['mid']);
    $sub_query = $this->database->select('entity_usage', 'eu');
    $sub_query->fields('eu', ['target_id']);
    $sub_query->condition('target_type', 'media');
    $sub_query->distinct();
    $query->condition('m.mid', $sub_query, 'NOT IN');
    $query->addTag('agoramedia_unreferenced_media');

    return $query->execute()->fetchCol();
  }

}
