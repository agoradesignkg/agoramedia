<?php

namespace Drupal\agoramedia\OEmbed;

use Drupal\media\OEmbed\Resource;
use Drupal\media\OEmbed\ResourceFetcher;

/**
 * Extends the core media oEmbed resource fetcher.
 *
 * It scales up the video's width and height, if needed.
 */
class HqResourceFetcher extends ResourceFetcher {

  /**
   * {@inheritdoc}
   */
  protected function createResource(array $data, $url) {
    if ($data['type'] == Resource::TYPE_VIDEO && $data['provider_name'] == 'YouTube') {
      if (!empty($data['width']) && !empty($data['height'])) {
        $target_width = \Drupal::config('agoramedia.settings')->get('youtube_oembed_width');
        if (!empty($target_width) && bccomp($target_width, $data['width']) > 0) {
          $scale = bcdiv($target_width, $data['width'], 6);
          $target_height = bcmul($data['height'], $scale);
          $data['width'] = $target_width;
          $data['height'] = $target_height;
        }
      }
    }
    // Create the resource.
    return parent::createResource($data, $url);
  }

}
